

**Welcome to the Logit Project**
--------------------------------

**This project is a WIP.**

What is Logit ?
---------------
Logit aims to be a simple tool that allow you to write logs and affiliate them with tags. It also allows you to search through your created logs by date, keywords and tags in order to read them or edit them.
Logit is made on my spare time just for fun.

How to use ?
------------
Logit is a simple CLI-program that has only some few commands.
*[Full Documentation to come]*