/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package log;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.codec.digest.DigestUtils;
import properties.PropertiesManager;

/**
 * The Log class.<br>
 * A Log is made of :<br>
 *  String id<br>
 *  String date<br>
 *  String text<br>
 *  TreeSet tag (See {@link Tag})<br>
 * 
 * 
 * @todo : Put home in a .properties file.
 * @author Theo Nguyen
 */
public class Log implements Serializable{
    /**
     * Format use for the dates.
     */
    private transient static final String format = "dd-MM-YYYY hh:mm";
    /**
     * Date Formater.
     */
    private transient static final DateFormat dateFormat = new SimpleDateFormat(format);
    /**
     * Id of a Log. Made out of SHA1 of the Log String representation.
     */
    private String id;
    /**
     * Date of the Log. Created automatically on creation.
     */
    private String date;
    /**
     * Text of the Log.
     */
    private String text;
    /**
     * TreeSet of tags.
     * See {@link Tag}
     */
    private Set<Tag> tags;
    
    /**
        Default empty constructor.
    */
    public Log(){
        
    }
    
    /**
     * Basic constructor of a Log, with only a text.
     * @param text_ The text for the log.
     */
    public Log(String text_){
        this.text = text_;
        this.date = Log.dateFormat.format(new Date());      
        this.tags = new TreeSet<Tag>();
        
        this.id = DigestUtils.sha1Hex(this.toString());
    }
    
    /**
     * Complete constructor of a Log.
     * @param text_ The text of the Log.
     * @param tags_ The list of tag.
     */
    public Log(String text_, Collection<Tag> tags_){
        this(text_);
        
        this.tags = new TreeSet<Tag>(tags_);
    }
     
    /**
     * Return the Log's file path.
     * @return String representation of the log's path.
     */
    public String getLogLocation(){
        StringBuilder path = new StringBuilder(PropertiesManager.get_HOME_());
        
        for(Tag tag : getTags()){
            path.append("\\");
            path.append(tag.toString());
        }
        path.append("\\");
        return path.toString();
    }
    
    /**
     * Return the Log file name.
     * @return String representation of the log file's name
     */
    public String getLogFileName(){
        StringBuilder filename = new StringBuilder(getId());
        filename.append(PropertiesManager.get_EXTENSION_());
        
        return filename.toString();
    }
    
    /**
     * Return the full path of the log (Log location + log filename)
     * @return String representation
     */
    public String getFullPath(){
        return getLogLocation().concat(getLogFileName());        
    }
    
    @Override
    public int hashCode(){
        return this.toString().hashCode();
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o)
            return true;
        if(o == null)
            return false;
        if(getClass() != o.getClass())
            return false;
        
        Log log_ = (Log)o;
        
        return this.getId().equals(log_.getId());
    }
    
    
    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        
        str.append("["+this.id+"]");
        str.append("\n");
        str.append("["+this.date+"]");
        str.append(" ");
        
        if(!this.tags.isEmpty()){
            str.append(this.tags.toString());
            
        }
        str.append("\n");
        str.append(this.text);
        
        return str.toString();
    }       
    
    /**
     * Get the id of the Log.
     * See {@link Log#id}
     * @return This Log's Id
     */
    public String getId() {
        return id;
    }

    /**
     * Get the date of the Log.
     * See {@link Log#date}
     * @return String The String representation of the date.
     */
    public String getDate() {
        return date;
    }

    /**
     * Set the date
     * See {@link Log#date}
     * @param date The String representation of the date
     */
    public void setDate(String date) {
        this.date = date;
    }
    
    /**
     * Set the date
     * See {@link Log#date}
     * @param date The date
     */
    public void setDate(Date date){
        this.date = dateFormat.format(date);
    }

    /**
     * Return the text of the Log
     * See {@link Log#text}
     * @return The String representation of the Log
     */
    public String getText() {
        return text;
    }

    /**
     * Set the text of the Log
     * See {@link Log#text}
     * @param text The Log's text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Return a Set containing this Log's Tag
     * See {@link Tag}
     * @return A Set of Tag
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Set this Log's Tag
     * See {@link Tag}
     * @param tags A Collection of Tag
     */
    public void setTags(Collection<Tag> tags) {
        this.tags = new TreeSet<Tag>(tags);
    }
    
    
}
