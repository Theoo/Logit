/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package log;

import java.io.Serializable;

/**
 *  Un tag = un dossier.<br>
 *  The tag class.<br>
 *  A Tag is a String.<br>
 *  A tag start with an upper case letter <br>
 *  A tag is only one word.<br>
 * 
 *  @author Theo Nguyen
 */
public class Tag implements Comparable<Tag>, Serializable{
    /**
     * The tag.
     */
    private String tag;
    
    /*
        Default empty constructor.
        Instantiate an empty String for the Tag.
    */
    public Tag(){
        this.tag = "";
    }
    
    /*
        Constructor with a String. 
        @param tag_ The string representation of the tag.
    */
    public Tag(String tag_){
        if(tag_.length() > 0)
            tag_ = tag_.substring(0, 1).toUpperCase() + tag_.substring(1, tag_.length());        
        this.tag = tag_.substring(0,(tag_.indexOf(" ")!=-1?
                                        tag_.indexOf(" "):tag_.length()
                                    ) 
                                );
    }

    /**
     * Return the Tag(s string.
     * See {@link Tag#tag}
     * @return The tag.
     */
    public String getTag() {
        return tag;
    }

    /**
     * Set the tag
     * See {@link Tag#tag}
     * @param tag_ The tag 
     */
    public void setTag(String tag_ ) {
        if(tag_.length() > 0)
            tag_ = tag_.substring(0, 1).toUpperCase() + tag_.substring(1, tag_.length());
        this.tag = tag_.substring(0,(tag_.indexOf(" ")!=-1?
                                        tag_.indexOf(" "):tag_.length()
                                    ) 
                                );
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o)
            return true;
        if(o == null)
            return false;
        if(getClass() != o.getClass())
            return false;
        
        Tag tag_ = (Tag)o;
        
        return this.tag.equals(tag_.getTag());
    }
    
    @Override
    public String toString(){
        return this.tag;        
    }

   
    @Override
    public int hashCode(){
        return this.tag.hashCode();
    }

    @Override
    public int compareTo(Tag o) {
        return this.tag.compareTo(o.getTag());
    }
    
    
}
