/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filesmanager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import log.Log;

/**
 *
 * @author Jakarta
 */
public class Serializer {
    
    public static boolean serialize(Log log){
        
        File folder = new File(log.getLogLocation());
        if(!folder.exists())
            folder.mkdirs();
        
        try{
            File logFile = new File(log.getFullPath());
            if(!logFile.exists())
                logFile.createNewFile();
            
            FileOutputStream fileOut = new FileOutputStream(logFile);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(log);
            out.close();
            fileOut.close();
                        
        }catch(IOException i){
            return false;
        }   
        
        return true;        
    }
    
    public static Log deserialize(String path) 
            throws IOException, ClassNotFoundException{
        
        Log log = null;
        FileInputStream fileIn = new FileInputStream(path);
        ObjectInputStream in = new ObjectInputStream(fileIn);
        log = (Log) in.readObject();
        in.close();
        fileIn.close();

        return log;
    }
}
