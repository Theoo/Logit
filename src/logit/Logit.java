/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logit;

import filesmanager.Serializer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import log.Log;
import log.Tag;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
/**
 *
 * @author Jakarta
 */
public class Logit {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException, ParseException {
       
        //logit log -m "Messagfe du log" -t "tag1 tag2 tag3"
       
       //java -jar "Logit.jar" log -m "Mon premier log en cmd" -t "tag1 tag2"

        if(args[0].equals("log")){
            Options logOpt = new Options();
            Option msgOpt = new Option("m", true, "Your log message.");
            Option tagOpt = new Option("t", true, "Your log's tag(s). Separate them with a space.");
            tagOpt.setRequired(false);
            tagOpt.setArgs(Option.UNLIMITED_VALUES);
            logOpt.addOption(msgOpt);           
            logOpt.addOption(tagOpt);

            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse( logOpt, args);
            
            List<Tag> tags = new ArrayList<Tag>();
            for (String s : cmd.getOptionValues("t"))
                tags.add(new Tag(s));
            
            Log log = new Log(cmd.getOptionValue("m"), tags);
            
            boolean bool = Serializer.serialize(log);
            if(bool)
                System.out.println("Log written succesfully");
            else
                System.out.println("Error while trying to write log. Please retry.");
        }
        
        else if(args[0].equals("show")){
            
        }
        
        
        
        
        
        
        /*
        
       List<Tag> tags = new ArrayList<Tag>();
       tags.add(new Tag("leSuperTag"));
       tags.add(new Tag("TropBien"));
       tags.add(new Tag("AndAnOtherOne"));
       
       Log log3 = new Log("Debut du projet Logit ! OHLALALALA", tags);
       
       
       boolean response1 = Serializer.serialize(log3);
       System.out.println(response1);
       
       
       Log log = Serializer.deserialize(log3.getFullPath());
       
       System.out.println(log.toString());
       
       
       System.out.println("-----------------------------------------");
       
       List<Tag> tags2 = new ArrayList<Tag>();
       tags2.add(new Tag("AndAnOtherOne"));
       tags2.add(new Tag("leSuperTag"));
       
       Log log2 = new Log("Un autre log", tags2);
       Response response2 = Serializer.serialize(log2);
       System.out.println(response2.toString());
       
       Log logx = null;
       Response responsex = null;
       Object valuex = Serializer.deserialize(log2.getFullPath());
       
       
       if(valuex instanceof Response){
           responsex = (Response) valuex;
           System.out.println(responsex.toString());
       }           
       else{
           logx = (Log) valuex;
           System.out.println(logx.toString());
       }
       
       */
       
       
       
       
    }
}