/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 *
 * @author Jakarta
 */
public class PropertiesManager {
    private static final String _PATH_ = "D:\\Logit\\src\\properties\\plogit.properties";    
    private static final String _HOME_STR = "_HOME_";
    private static final String _EXTESION_STR_ = "_EXTENSION_";
    /**
     * Load the .properties file and return it.
     * @return Properties object that contains all the properties in the loaded
     * file
     */
    private static Properties loadProperties(){
        InputStream inputStream = null;
        Properties properties = new Properties();        
        try {
            inputStream = new FileInputStream(_PATH_);
            properties.load(inputStream);
            inputStream.close();
        }catch (Exception e) {
                System.out.println("Exception: " + e);
        }         
        return properties;
    }
    
    /*
         _HOME_ 
    */
    
    /**
     * Return the value of the _HOME_ property.
     * @return String representation of _HOME_ property
     */
    public static String get_HOME_(){
        return loadProperties().getProperty(_HOME_STR);
    }
    
    /**
     * Write the new value for the _HOME_ property in the .property file
     * @param property The String value for _HOME_ property
     */
    public static void set_HOME_(String property){
        Properties properties = loadProperties();
        OutputStream outputStream;
        properties.setProperty(_HOME_STR, property);
        try{            
            outputStream = new FileOutputStream(_PATH_);
            properties.store(outputStream, 
                    "Properties file for the Logit Project");
            
        }catch (Exception e) {
             System.out.println("Exception: " + e);
        } 
    }
    
    
        
    /*
         _EXTENSION_ 
    */
    
    /**
     * Return the value of the _EXTENSION_ property.
     * @return String representation of _EXTENSION_ property
     */
    public static String get_EXTENSION_(){
        return loadProperties().getProperty(_EXTESION_STR_);
    }    
    
    /**
     * Write the new value for the _EXTENSION_ property in the .property file
     * @param property The String value for _EXTENSION_ property
     */
    public static void get_EXTENSION_(String property){
        Properties properties = loadProperties();
        OutputStream outputStream;
        properties.setProperty(_EXTESION_STR_, property);
        try{            
            outputStream = new FileOutputStream(_PATH_);
            properties.store(outputStream,
                    "Properties file for the Logit Project");
            
        }catch (Exception e) {
             System.out.println("Exception: " + e);
        } 
    }
}
